<?php

namespace App\Exports;

use App\Models\Manifiesto;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ManifiestosExport implements FromView
{
    public $manifiesto;
    public function __construct(int $manifiesto)
    {
        $this->manifiesto = $manifiesto;
    }
    public function view(): View
    {
        $manifiesto = Manifiesto::where('id', $this->manifiesto)
        ->with(
            'guias',
            'guias.guia',
            'guias.guia.articulos',
            'guias.guia.courier',
            'guias.guia.oficinaCourier',
            'guias.guia.oficina',
            'guias.guia.destinatario',
            'guias.guia.cliente',
        )->first();

        return view('exports.manifiesto', [
            'manifiesto' => $manifiesto
        ]);
    }
}
