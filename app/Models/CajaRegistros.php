<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CajaRegistros extends Model
{
    use HasFactory;

    protected $fillable = [
        'idCaja',
        'concepto',
        'tipo',
        'monto',
        'tipoMoneda'
    ];
}
