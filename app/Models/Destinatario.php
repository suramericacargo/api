<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Destinatario extends Model
{
    use HasFactory;

    protected $fillable = [
        'idCliente',
        'idEmpresa',
        'idTipoDocumento',
        'documento',
        'nombres',
        'apellidos',
        'telefono',
        'correo',
        'estado',
        'ciudad',
        'direccion',
    ];
}
