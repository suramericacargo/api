<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    use HasFactory;

    protected $fillable = [
        'idEmpresa',
        'idTipoDocumento',
        'documento',
        'nombres',
        'apellidos',
        'telefono',
        'correo',
        'estado',
        'ciudad',
        'direccion',
        'nacionalidad'
    ];
}
