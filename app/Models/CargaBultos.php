<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CargaBultos extends Model
{
    use HasFactory;

    protected $fillable = [
        'idCarga',
        'precinto',
        'manifiestos',
        'alto',
        'ancho',
        'largo',
        'peso',
        'pesoVolumetrico',
    ];
}
