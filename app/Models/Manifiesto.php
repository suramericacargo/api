<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Manifiesto extends Model
{
    use HasFactory;

    protected $fillable = [
        'idEmpresa',
        'idUsuario',
        'idOficina',
        'tipoGuia',
        'estado',
        'idCarga',
        'fecha',
    ];

    public function guias()
    {
        return $this->hasMany(ManifiestoGuias::class, 'idManifiesto', 'id');
    }

    public function trackings()
    {
        return $this->hasMany(ManifiestoTracking::class, 'idManifiesto', 'id');
    }

    public function oficina()
    {
        return $this->hasOne(Oficina::class, 'id', 'idOficina');
    }

    protected $casts = [
        'created_at' => 'datetime:d-m-Y',
    ];
}
