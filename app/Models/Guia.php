<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Guia extends Model
{
    use HasFactory;

    protected $fillable = [
        'tipoGuia',
        'idUsuario',
        'idEmpresa',
        'idOficina',
        'idCliente',
        'idDestinatario',
        'idCourier',
        'idOficinaCourier',
        'idTasa',
        'idManifiesto',
        'idCarga',
        'idBulto',
        'guia_courier',
        'alto',
        'ancho',
        'largo',
        'peso_volumetrico',
        'm3',
        'ft3',
        'peso',
        'costo',
        'divisa',
        'divisaSimbolo',
        'paisOrigen',
        'paisDestino',
        'montoSeguro',
        'entregaPP',
        'pOrigen',
        'pDestio',
        'idCaja'
    ];

    public function destinatario()
    {
        return $this->hasOne(Destinatario::class, 'id', 'idDestinatario');
    }

    public function oficina()
    {
        return $this->hasOne(Oficina::class, 'id', 'idOficina');
    }

    public function cliente()
    {
        return $this->hasOne(Cliente::class, 'id', 'idCliente');
    }

    public function courier()
    {
        return $this->hasOne(Courier::class, 'id', 'idCourier');
    }

    public function oficinaCourier()
    {
        return $this->hasOne(OficinaCourier::class, 'id', 'idOficinaCourier');
    }

    public function articulos()
    {
        return $this->hasMany(GuiaArticulo::class, 'idGuia', 'id');
    }

    public function tracking()
    {
        return $this->hasMany(GuiaTracking::class, 'idGuia', 'id');
    }

    public function getCreatedAtAttribute($value)
    {
        return $value ? Carbon::parse($value)->format('d-m-Y H:i:s') : null ;
    }

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
    ];
}
