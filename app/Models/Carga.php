<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Carga extends Model
{
    use HasFactory;

    protected $fillable = [
        'idUsuario', 'tipoCarga', 'fechaSalida', 'estado'
    ];

    public function manifiestos()
    {
        return $this->hasMany(CargaManifiestos::class, 'idCarga', 'id');
    }

    public function trackings() {
        return $this->hasMany(CargaTrackings::class, 'idCarga', 'id');
    }
}
