<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Caja extends Model
{
    use HasFactory;
    protected $fillable =[
        'idEmpresa',
        'idOficina',
        'idUsuario',
        'montoInicial',
        'montoFinal',
        'montoIngresos',
        'montoEgresos',
        'status',
        'montoDolaresInicial',
        'montoDolaresFinal',
        'montoSolesInicial',
        'montoSolesFinal',
        'ingresosSoles',
        'ingresosDolares',
        'egresosSoles',
        'egresosDolares',
        'tasaDolares'
    ];

    public function registros()
    {
        return $this->hasMany(CajaRegistros::class, 'idCaja', 'id');
    }

    public function cajero()
    {
        return $this->hasOne(User::class, 'id', 'idUsuario');
    }
    protected $casts = [
        'created_at' => 'datetime:Y-m-d',
    ];
}
