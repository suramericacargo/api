<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    use HasFactory;

    protected $fillable = [
        'categoria',
        'idEmpresa',
    ];

    public function sub_categorias()
    {
        return $this->hasMany(SubCategoria::class, 'idCategoria', 'id');
    }
}
