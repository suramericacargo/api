<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TasaEnvio extends Model
{
    use HasFactory;

    protected $fillable = [
        'idEmpresa',
        'idPais',
        'divisa',
        'divisaSimbolo',
        'tipoEnvio',
        'tasa',
        'precioFijo',
        'precioCalculado',
        'nombre'
    ];
}
