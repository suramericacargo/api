<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GuiaTracking extends Model
{
    use HasFactory;

    protected $fillable = [
        'idUsuario',
        'idGuia',
        'estado',
        'descripcion',
    ];

    public function getCreatedAtAttribute($value)
    {
        return $value ? Carbon::parse($value)->format('d-m-Y H:i:s') : null ;
    }
}
