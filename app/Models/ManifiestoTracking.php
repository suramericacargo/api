<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ManifiestoTracking extends Model
{
    use HasFactory;

    protected $fillable = [
        'idManifiesto',
        'estado',
    ];

    protected $casts = [
        'created_at' => 'datetime:d-m-Y',
    ];
}
