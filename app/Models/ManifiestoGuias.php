<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ManifiestoGuias extends Model
{
    use HasFactory;

    protected $fillable = [
        'idManifiesto',
        'idGuia',
    ];

    public function guia()
    {
        return $this->hasOne(Guia::class, 'id', 'idGuia');
    }
}
