<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ClienteController extends Controller
{

    public function index()
    {
        if(Auth::user()->typeUser == 1)
        {
            $clientes = Cliente::all();
        }
        else{
            $clientes = Cliente::where('idEmpresa', Auth::user()->idEmpresa)->get();
        }

        return response()->json([
            'data' => $clientes,
            'message' => 'Clientes obtenidos con exito',
            'error' => false
        ]);
    }
    public function newGuia(Request $request)
    {
        $clientes = Cliente::where('idEmpresa', $request->user()->idEmpresa)->get();
        $clientes2 = [];
        foreach ($clientes as $cliente ) {
            $clientes2[] = array(
                'label' => $cliente->documento .' - '. $cliente->nombres .' '. $cliente->apellidos,
                'value' => $cliente->id
            );
        }

        return response()->json([
            'data' => $clientes2,
            'message' => 'Clientes obtenidos con exito!',
            'error' => false
        ]);
    }
    public function store(Request $request)
    {
        $cliente = Cliente::updateOrCreate(
            [
                'documento' => $request->documento,
            ],
            [
                'idEmpresa' => $request->user()->idEmpresa,
                'idTipoDocumento' => $request->tipoDocumento,
                'nombres' => strtoupper($request->nombre),
                'apellidos' => strtoupper($request->apellido),
                'telefono' => $request->telefono,
                'correo' => strtoupper($request->correo),
                'estado' => strtoupper($request->estado),
                'ciudad' => strtoupper($request->ciudad),
                'direccion' => strtoupper($request->direccion),
                'nacionalidad' => $request->nacionalidad
            ]
            );

        return response()->json([
            'data' => $cliente,
            'message' => 'Cliente Registrado con exito',
            'error' => false
        ]);
    }

    public function update(Request $request)
    {
        $cliente = Cliente::updateOrCreate(
            [
                'documento' => $request->documento,
            ],
            [
                'idEmpresa' => $request->user()->idEmpresa,
                'idTipoDocumento' => $request->idTipoDocumento,
                'nombres' => strtoupper($request->nombres),
                'apellidos' => strtoupper($request->apellidos),
                'telefono' => $request->telefono,
                'correo' => strtoupper($request->correo),
                'estado' => strtoupper($request->estado),
                'ciudad' => strtoupper($request->ciudad),
                'direccion' => strtoupper($request->direccion),
                'nacionalidad' => $request->nacionalidad
            ]
            );

        return response()->json([
            'data' => $cliente,
            'message' => 'Cliente Registrado con exito',
            'error' => false
        ]);
    }
    public function show($cliente)
    {
        $cliente = Cliente::where('documento', $cliente)->first();

        if($cliente != null)
        {
            $location = true;
        }
        else{
            $location = false;
        }

        return response()->json([
            'location' => $location,
            'data' => $cliente,
            'error' => false
        ]);
    }
}
