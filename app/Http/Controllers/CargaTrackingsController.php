<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCargaTrackingsRequest;
use App\Http\Requests\UpdateCargaTrackingsRequest;
use App\Models\CargaTrackings;

class CargaTrackingsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreCargaTrackingsRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(CargaTrackings $cargaTrackings)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(CargaTrackings $cargaTrackings)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateCargaTrackingsRequest $request, CargaTrackings $cargaTrackings)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(CargaTrackings $cargaTrackings)
    {
        //
    }
}
