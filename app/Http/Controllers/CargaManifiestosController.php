<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCargaManifiestosRequest;
use App\Http\Requests\UpdateCargaManifiestosRequest;
use App\Models\CargaManifiestos;

class CargaManifiestosController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreCargaManifiestosRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(CargaManifiestos $cargaManifiestos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(CargaManifiestos $cargaManifiestos)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateCargaManifiestosRequest $request, CargaManifiestos $cargaManifiestos)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(CargaManifiestos $cargaManifiestos)
    {
        //
    }
}
