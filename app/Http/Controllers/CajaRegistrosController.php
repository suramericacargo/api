<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCajaRegistrosRequest;
use App\Http\Requests\UpdateCajaRegistrosRequest;
use App\Models\CajaRegistros;

class CajaRegistrosController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreCajaRegistrosRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(CajaRegistros $cajaRegistros)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(CajaRegistros $cajaRegistros)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateCajaRegistrosRequest $request, CajaRegistros $cajaRegistros)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(CajaRegistros $cajaRegistros)
    {
        //
    }
}
