<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreOficinaRequest;
use App\Http\Requests\UpdateOficinaRequest;
use App\Models\Oficina;
use Illuminate\Http\Request;

class OficinaController extends Controller
{

    public function oficinasNew(Request $request)
    {
        $oficinas = Oficina::where('idEmpresa', $request->user()->idEmpresa)->get();
        $oficinas2 = [];

        foreach ($oficinas as $oficina ) {
            $oficinas2[] = array(
                'label' => $oficina->codigo.' - '.$oficina->nombre,
                'value' => $oficina->id
            );
        }

        return response()->json([
            'message' => 'Oficinas obtenidas con exito',
            'data' => $oficinas2,
            'error' => false
        ]);
    }
    public function index(Request $request)
    {
        return response()->json([
            'message' => 'Oficinas obtenidas con exito',
            'data' => Oficina::where('idEmpresa', $request->user()->idEmpresa)->get(),
            'error' => false
        ]);
    }

    public function store(Request $request)
    {
        $oficina = Oficina::create([
            'idEmpresa' => $request->idEmpresa,
            'codigo' => strtoupper($request->codigo),
            'nombre' => strtoupper($request->nombre),
            'pais' => strtoupper($request->pais),
            'ciudad' => strtoupper($request->ciudad),
            'direccion' => strtoupper($request->direccion),
            'telefono' => $request->telefono,
            'horario_atencion' => strtoupper($request->horario_atencion),
        ]);

        return response()->json([
            'message' => 'Oficina registrada con exito',
            'data' => $oficina,
            'error' => false
        ]);
    }

    public function show(Oficina $oficina)
    {
        //
    }

    public function update(Request $request, Oficina $oficina)
    {
        //
    }

    public function destroy(Oficina $oficina)
    {
        //
    }
}
