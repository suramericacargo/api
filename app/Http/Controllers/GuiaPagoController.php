<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreGuiaPagoRequest;
use App\Http\Requests\UpdateGuiaPagoRequest;
use App\Models\GuiaPago;

class GuiaPagoController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreGuiaPagoRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(GuiaPago $guiaPago)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(GuiaPago $guiaPago)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateGuiaPagoRequest $request, GuiaPago $guiaPago)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(GuiaPago $guiaPago)
    {
        //
    }
}
