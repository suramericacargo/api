<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCajaRequest;
use App\Http\Requests\UpdateCajaRequest;
use App\Models\Caja;
use App\Models\CajaRegistros;
use App\Models\Oficina;
use Illuminate\Http\Request;
use GuzzleHttp\Client;

class CajaController extends Controller
{
    public function index(Request $request)
    {
        if($request->user()->typeUser == 1)
        {
            $cajas = Caja::with('cajero')->get();
        }
        if($request->user()->typeUser == 2)
        {
            $cajas = Caja::where('idEmpresa', $request->user()->idEmpresa)->with('cajero')->get();
        }
        if($request->user()->typeUser == 3)
        {
            $cajas = Caja::where('idUsuario', $request->user()->id)->with('cajero')->get();
        }

        return response()->json([
            'data' => $cajas,
            'message' => 'Cajas obtenidas con exito',
            'error' => false
        ]);
    }

    public function aperturarCaja(Request $request)
    {
        $val = Caja::where('idUsuario',$request->user()->id)->where('status', 1)->count();
        if($val > 0)
        {
            return response()->json([
                'data' => Caja::where('idUsuario',$request->user()->id)->where('status', 1)->first(),
                'message' => 'Ya tiene una caja activa, hemos asignado esa caja para seguir trabajando',
                'error' => true
            ]);
        }


        if($request->user()->idOficina == 0 OR $request->user()->idOficina == null)
        {
            $oficina = Oficina::where('idEmpresa', $request->user()->idEmpresa)->first();
            $idOficina = $oficina->id;
        }
        else{
            $idOficina = $request->user()->idOficina;
        }

        $dolaresSoles = $request->montoDolares * $request->montoTasa;
        $totalInicio = $dolaresSoles + $request->montoSoles;

        $caja = Caja::create([
            'idEmpresa' => $request->user()->idEmpresa,
            'idOficina' => $idOficina,
            'idUsuario' => $request->user()->id,
            'montoInicial' => $totalInicio,
            'montoFinal' => $totalInicio,
            'montoIngresos' => 0,
            'montoEgresos' => 0,
            'status' => 1,
            'montoDolaresInicial' => $request->montoDolares,
            'montoDolaresFinal' => $request->montoDolares,
            'montoSolesInicial' => $request->montoSoles,
            'montoSolesFinal' => $request->montoSoles,
            'ingresosSoles' => 0,
            'ingresosDolares' => 0,
            'egresosSoles' => 0,
            'egresosDolares' => 0,
            'tasaDolares' => $request->montoTasa
        ]);

        return response()->json([
            'data' => $caja,
            'message' => 'Caja registrada con exito',
            'error' => false
        ]);
    }

    public function cerrarCaja(Request $request)
    {
        $caja = Caja::find($request->idCaja);
        $caja->status = 2;
        $caja->save();

        return response()->json([
            'data' => $caja,
            'message' => 'Caja cerrada con exito, puedes revisar el informe y packaglist en unos minutos',
            'error' => false
        ]);
    }

    public function transaccion(Request $request)
    {
        $registro = CajaRegistros::create([
            'idCaja' => $request->idCaja,
            'concepto' => $request->concepto,
            'tipo' => $request->tipo,
            'monto' => $request->monto,
            'tipoMoneda' => $request->tipoMoneda
        ]);

        $caja = Caja::find($request->idCaja);
        // Valida el tipo de moneda
        if($request->tipoMoneda == 'soles')
        {
            // Valida el tipo de transaccion
            if($request->tipo == 'ingreso')
            {
                $caja->ingresosSoles += $request->monto;
                $caja->montoSolesFinal += $request->monto;
            }
            else{
                $caja->egresosSoles += $request->monto;
                $caja->montoSolesFinal -= $request->monto;
            }
        }
        else{
            // Valida el tipo de transaccion
            if($request->tipo == 'ingreso')
            {
                $caja->ingresosDolares += $request->monto;
                $caja->montoDolaresFinal += $request->monto;
            }
            else{
                $caja->egresosDolares += $request->monto;
                $caja->montoDolaresFinal -= $request->monto;
            }
        }
        $caja->save();

        return response()->json([
            'data' => $registro,
            'message' => 'Transaccion Creada con exito',
            'error' => false
        ]);
    }

    public function getTasa()
    {
        // Obtener tasa para montos totales en soles
        $client = new Client();
        $response = $client->request('GET', "https://api.apis.net.pe/v1/tipo-cambio-sunat");
        $data = json_decode($response->getBody());
        return $data;
    }

    public function detalle($caja)
    {
        $caja = Caja::where('id', $caja)->with('registros')->first();

        return response()->json([
            'data' => $caja,
            'message' => 'Caja obtenida con exito',
            'error' => false
        ]);
    }
}
