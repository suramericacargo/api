<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTasaEnvioRequest;
use App\Http\Requests\UpdateTasaEnvioRequest;
use App\Models\TasaEnvio;
use Illuminate\Http\Request;

class TasaEnvioController extends Controller
{
    public function index(Request $request)
    {
        return response()->json([
            'message' => 'Tasas de envio Obtenidos con exito',
            'data' => TasaEnvio::where('idEmpresa', $request->user()->idEmpresa)->get(),
            'error' => false
        ]);
    }

    public function crear(Request $request, $tipoGuia)
    {
        $tasas = TasaEnvio::where('idEmpresa', $request->user()->idEmpresa)->where('tipoEnvio', $tipoGuia)->get();
        $tasas2 = [];
        foreach ($tasas as $tasa ) {
            $tasas2[] = array(
                'label' => $tasa->nombre,
                'value' => $tasa
            );
        }
        return response()->json([
            'message' => 'Tasas de envio Obtenidos con exito',
            'data' => $tasas2,
            'error' => false
        ]);
    }

    public function store(Request $request)
    {
        if($request->tipoPrecio == 1)
        {
            $precioFijo = 1;
            $precioCalculado = 0;
        }
        else{
            $precioFijo = 0;
            $precioCalculado = 1;
        }
        $tasa = TasaEnvio::create([
            'idEmpresa' => $request->user()->idEmpresa,
            'idPais' => $request->idPais,
            'divisa' => $request->divisa,
            'divisaSimbolo' => $request->divisaSimbolo,
            'tipoEnvio' => $request->tipoEnvio,
            'tasa' => $request->tasa,
            'precioFijo' => $precioFijo,
            'precioCalculado' => $precioCalculado,
            'nombre' => strtoupper($request->nombre)
        ]);

        return response()->json([
            'message' => 'Tasa de envio registrado con exito',
            'data' => $tasa,
            'error' => false
        ]);
    }

    public function update(Request $request, TasaEnvio $tasaEnvio)
    {
        if($request->tipoPrecio == 1)
        {
            $precioFijo = 1;
            $precioCalculado = 0;
        }
        else{
            $precioFijo = 0;
            $precioCalculado = 1;
        }
        $tasaEnvio->precioFijo = $precioFijo;
        $tasaEnvio->precioCalculado = $precioCalculado;
        $tasaEnvio->precioCalculado = $precioCalculado;
        $tasaEnvio->tasa = $request->tasa;
        $tasaEnvio->tipoEnvio = $request->tipoEnvio;
        $tasaEnvio->divisa = $request->divisa;
        $tasaEnvio->divisaSimbolo = $request->divisaSimbolo;
        $tasaEnvio->idPais = $request->idPais;
        $tasaEnvio->nombre = strtoupper($request->nombre);
        $tasaEnvio->save();
    
        return response()->json([
            'message' => 'Tasa de envio actualizada con exito',
            'data' => $tasaEnvio,
            'error' => false
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(TasaEnvio $tasaEnvio)
    {
        return response()->json([
            'message' => 'Tasa de envio registrado con exito',
            'data' => $tasaEnvio,
            'error' => false
        ]);
    }

}
