<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreGuiaRequest;
use App\Http\Requests\UpdateGuiaRequest;
use App\Models\Caja;
use App\Models\CajaRegistros;
use App\Models\Empresa;
use App\Models\Guia;
use App\Models\GuiaArticulo;
use App\Models\GuiaPago;
use App\Models\GuiaTracking;
use App\Models\Oficina;
use App\Models\Paises;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GuiaController extends Controller
{
    public function dashboard(Request $request, $tipoGuia)
    {
        if($request->user()->typeUser == 3)
        {
            return response()->json([
                'guiasDay' => Guia::where('tipoGuia', $tipoGuia)->where('idUsuario', $request->user()->id)->whereDate('created_at', date('Y-m-d'))->count(),
                'guiasTotal' => Guia::where('tipoGuia', $tipoGuia)->where('idUsuario', $request->user()->id)->count(),
                'guias' => Guia::where('tipoGuia', $tipoGuia)->where('idUsuario', $request->user()->id)->with('destinatario', 'oficina')->orderBy('id', 'desc')->get(),
                'error' => false
            ]);
        }
        if($request->user()->typeUser == 2)
        {
            return response()->json([
                'guiasDay' => Guia::where('tipoGuia', $tipoGuia)->where('idEmpresa', $request->user()->idEmpresa)->whereDate('created_at', date('Y-m-d'))->count(),
                'guiasTotal' => Guia::where('tipoGuia', $tipoGuia)->where('idEmpresa', $request->user()->idEmpresa)->count(),
                'guias' => Guia::where('tipoGuia', $tipoGuia)->where('idEmpresa', $request->user()->idEmpresa)->with('destinatario', 'oficina')->orderBy('id', 'desc')->get(),
                'error' => false
            ]);
        }
        if($request->user()->typeUser == 1)
        {
            return response()->json([
                'guiasDay' => Guia::where('tipoGuia', $tipoGuia)->whereDate('created_at', date('Y-m-d'))->count(),
                'guiasTotal' => Guia::where('tipoGuia', $tipoGuia)->count(),
                'guias' => Guia::where('tipoGuia', $tipoGuia)->with('destinatario', 'oficina')->orderBy('id', 'desc')->get(),
                'error' => false
            ]);
        }
    }
    public function store(Request $request)
    {
        $idOficina = $request->user()->idOficina;
        $pOrigen = Paises::find($request->pOrigen);
        $pDestino = Paises::find($request->pDestino);

        if($request->entregaPP == 1)
        {
            $idCourier = 0;
            $idOficinaCourier = 0;
            $entregaPP = 1;
        }
        else{
            $idCourier = 0;
            $idOficinaCourier = $request->idCourier;
            $entregaPP = 0;
        }

        $pesoVolumetrico = $request->alto * $request->ancho * $request->largo / 5000;
        $m3 = $request->alto * $request->ancho * $request->largo / 1000000;
        $ft3 = $request->alto * $request->ancho * $request->largo * 0.0000353147;

        $guia = Guia::create([
            'pOrigen' => $pOrigen->bandera,
            'pDestio' => $pDestino->bandera,
            'tipoGuia' => $request->tipoGuia,
            'idUsuario' => $request->user()->id,
            'idEmpresa' => $request->user()->idEmpresa,
            'idOficina' => $idOficina,
            'idCliente' => $request->idCliente,
            'idDestinatario' => $request->idDestinatario,
            'idCourier' => $idCourier,
            'idOficinaCourier' => $idOficinaCourier,
            'idTasa' => $request->idTasa,
            'alto' => $request->alto,
            'ancho' => $request->ancho,
            'largo' => $request->largo,
            'peso_volumetrico' => $pesoVolumetrico,
            'm3' => $m3,
            'ft3' => $ft3,
            'peso' => $request->peso,
            'costo' => $request->costo,
            'divisa' => $request->divisa,
            'divisaSimbolo' => $request->divisaSimbolo,
            'montoSeguro' => $request->montoSeguro,
            'entregaPP' => $entregaPP,
            'idCaja' => $request->idCaja,
            'paisOrigen' => $pOrigen->nombre,
            'paisDestino' => $pDestino->nombre
        ]);

        $oficina = Oficina::find($idOficina);

        $trackingInicial = GuiaTracking::create([
            'idUsuario' => $request->user()->id,
            'idGuia' => $guia->id,
            'estado' => 'Creacion',
            'descripcion' => "Paquete recepcionado en oficina ( $oficina->codigo-$oficina->nombre  / $oficina->direccion )"
        ]);

        $concepto = "Facturacion por guia ".strtoupper($request->tipoGuia). " Registro: ". $guia->id;

        // Aqui agregamos transaccion
        $this->transaccion($request->idCaja, $concepto, 'ingreso', $request->costo, $request->divisa);

        $articulos = $request->articulos;

        foreach ($articulos as $articulo ) {
            $art = GuiaArticulo::create([
                'idGuia' => $guia->id,
                'cantidad' => $articulo['cantidad'],
                'bateria' => 0,
                'monto' => $articulo['precio'],
                'producto' => strtoupper($articulo['nombre'])
            ]);
        }

        $pagos = $request->pagos;

        foreach ($pagos as $pago ) {
            $pag = GuiaPago::create([
                'idGuia' => $guia->id,
                'tipoPago' => strtoupper($pago['tipoPago']),
                'divisa' => $request->divisa,
                'divisaSimbolo' => $request->divisaSimbolo,
                'monto' => $pago['montoPago']
            ]);
        }

        return response()->json([
            'data' => $guia,
            'message' => 'Guia creada con exito',
            'error' => false
        ]);
    }

    public function transaccion($caja, $concepto, $tipo, $monto, $tipoMoneda)
    {
        if($tipoMoneda == 'Soles')
        {
                $tipoMoneda = 'soles';
        }

        $registro = CajaRegistros::create([
            'idCaja' => $caja,
            'concepto' => $concepto,
            'tipo' => $tipo,
            'monto' => $monto,
            'tipoMoneda' => $tipoMoneda
        ]);

        $caja = Caja::find($caja);
        // Valida el tipo de moneda
        if($tipoMoneda == 'soles')
        {
                $caja->ingresosSoles += $monto;
                $caja->montoSolesFinal += $monto;
        }
        else{
                $caja->ingresosDolares += $monto;
                $caja->montoDolaresFinal += $monto;
        }
        $caja->save();
    }

    public function etiquetaPdf($guia)
    {
        $guia = Guia::where('id', $guia)->with('cliente', 'destinatario', 'oficina', 'courier', 'oficinaCourier')->first();
        $empresa = Empresa::find($guia->idEmpresa);
        // return $guia;
        $pdf = \PDF::loadView('pdfs.etiqueta', ['guia' => $guia, 'empresa' => $empresa]);
        $pdf->setPaper('a5', 'landscape');
        $pdf->render();
        return $pdf->stream("etiqueta.pdf") ;
    }

    public function guiaPdf($guia, Request $request)
    {
        $guia = Guia::where('id', $guia)->with('cliente', 'destinatario', 'oficina', 'courier', 'oficinaCourier', 'articulos')->first();
        $empresa = Empresa::find($guia->idEmpresa);
        // return $guia;
        $pdf = \PDF::loadView('pdfs.guia', ['guia' => $guia, 'empresa' => $empresa]);
        $pdf->render();
        return $pdf->stream("Guia-".$guia->oficina->codigo.$guia->id.".pdf") ;
    }

    public function TicketPdf($guia)
    {
        $guia = Guia::where('id', $guia)->with('cliente', 'destinatario', 'oficina', 'courier', 'oficinaCourier', 'articulos')->first();
        $empresa = Empresa::find($guia->idEmpresa);
        // return $guia;
        $pdf = \PDF::loadView('pdfs.tickect', ['guia' => $guia, 'empresa' => $empresa]);
        $pdf->setPaper('a5', 'landscape');
        $pdf->set_paper(array(0,0,227,1000));
        $pdf->render();
        return $pdf->stream("Ticket-".$guia->oficina->codigo.$guia->id.".pdf") ;
    }

    public function getGuia(Request $request)
    {
        $guia = Guia::where('id', $request->guia)->with('tracking')->first();
        if($guia == null)
        {
            return response()->json([
                'error' => true,
                'message' => 'Numero de guia no encontrado'
            ]);
        }
        else{
            return response()->json([
                'error' => false,
                'message' => 'Guia Obtenido con exito',
                'tracking' => $guia->tracking
            ]);
        }
    }
}
