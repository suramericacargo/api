<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCategoriaRequest;
use App\Http\Requests\UpdateCategoriaRequest;
use App\Models\Categoria;
use App\Models\SubCategoria;
use Illuminate\Http\Request;

class CategoriaController extends Controller
{
    public function index(Request $request)
    {
        return response()->json([
            'message' => 'Categorias Obtenidas con exito',
            'data' => Categoria::withCount('sub_categorias')->where('idEmpresa', $request->user()->idEmpresa)->get(),
            'error' => false
        ]);
    }
    public function store(Request $request)
    {
        $categoria = Categoria::create([
            'categoria' => $request->categoria,
            'idEmpresa' => $request->user()->idEmpresa
        ]);

        return response()->json([
            'message' => 'Categoria Craeada con exito',
            'data' => $categoria,
            'error' => false
        ]);
    }

    public function show($categoria)
    {
        $categoria = Categoria::where('id', $categoria)->with('sub_categorias')->first();

        return response()->json([
            'message' => 'Categoria Obtenida con exito',
            'data' => $categoria,
            'error' => false
        ]);
    }
}
