<?php

namespace App\Http\Controllers;

use App\Models\{Carga, CargaManifiestos, Manifiesto, Guia, ManifiestoTracking, CargaTrackings, GuiaTracking };
use Illuminate\Http\Request;
use Shuchkin\SimpleXLSX;

class CargaController extends Controller
{
    public function index()
    {
        return response()->json([
            'data' => Carga::withCount('manifiestos')->get(),
            'error' => false,
            'message' => 'Cargas obtenidas con exito'
        ]);
    }

    public function store(Request $request)
    {

        $carga = Carga::create([
            'idUsuario' => $request->user()->id,
            'tipoCarga' => $request->tipoEnvio,
            'fechaSalida' => date('Y-m-d'),
            'estado' => 'CREADA'
        ]);

        foreach ($request->manifiestos as $manifiesto) {
            $manifiestoData = Manifiesto::find($manifiesto);
            // Asigna la carga al manifiesto
            $manifiestoData->idCarga = $carga->id;
            $manifiestoData->save();

            // Asigna la relacion carga manifiesto
            $rel = CargaManifiestos::create([
                'idCarga' => $carga->id,
                'idManifiesto' => $manifiesto
            ]);

            // consulta las guias
            $guias = Guia::where('idManifiesto', $manifiesto)->get();

            // asigna el nuevo tracking a las guias
            foreach ($guias as $guia) {
                $this->trackingGuias('Aduana origen', $request->user()->id, $guia->id);
            }

            // asigna el tracking al manifiesto
            $tracking = ManifiestoTracking::create([
                'idManifiesto' => $manifiesto,
                'estado' => 'Aduana origen'
            ]);
        }

         // asigna el tracking a la carga
         $track = CargaTrackings::create([
            'idCarga' => $carga->id,
            'estado' => 'Aduana origen'
        ]);

        return response()->json([
            'data' => $carga,
            'error' => false,
            'message' => 'Carga Nº: '.$carga->id. ' generado con exito'
        ]);
    }


    public function trackingGuias($estado, $user, $guia)
    {
        if($estado == 'Almacén') //manifiesto
        { $estado = ['estado' => 'Almacén', 'texto' => 'Su paquete ha sido envalijado en Peru, y en pronto será enviado a su destino Venezuela.']; }
        if($estado == 'Aduana origen') //carga
        { $estado = ['estado' => 'Aduana origen', 'texto' => 'Su paquete ha sido entregado a la aduana en Peru donde se le cargarán los aranceles de salida del país o aranceles. ']; }
        if($estado == 'En Transito') // al dia siguiente
        { $estado = ['estado' => 'En Transito', 'texto' => 'Su paquete paquete se encuentra en translado al destino en Venezuela.']; }
        if($estado == 'Almacén ADT')
        { $estado = ['estado' => 'Almacén ADT', 'texto' => 'Mercancía no despachada a la espera de recibir el transbordo hacia Venezuela.']; }
        if($estado == 'Aduana destino') // 2 dias aduana destino
        { $estado = ['estado' => 'Aduana destino', 'texto' => 'Su paquete ha llegado a la aduana en Venezuela donde se le cargarán los aranceles de ingreso al país y será tramitado para el envío al Centro de Distribución.']; }
        if($estado == 'Recibido en Distribución')
        { $estado = ['estado' => 'Recibido en Distribución', 'texto' => 'El paquete se encuentra en el Centro de Distribución para su envío a la Sucursal seleccionada.']; }
        if($estado == 'Transferido')
        { $estado = ['estado' => 'Transferido', 'texto' => 'Su paquete fue enviado a la sucursal de retiro. ']; }
        if($estado == 'Disponible')
        { $estado = ['estado' => 'Disponible', 'texto' => 'Ya puedes retirar su paquete. ']; }
        if($estado == 'Entregado')
        { $estado = ['estado' => 'Entregado', 'texto' => 'Su paquete ha sido entregado a su destinatario!. ']; }

        $tracking = GuiaTracking::create([
            'idUsuario' => $user,
            'idGuia' => $guia,
            'estado' => $estado['estado'],
            'descripcion' => $estado['texto'],
        ]);
    }

    public function manifiestosPendientes(Request $request, $tipoManifiesto)
    {
        $manifiestos = Manifiesto::where('idCarga', null)->where('tipoGuia', $tipoManifiesto)->get();

        return response()->json([
            'data' => $manifiestos,
            'error' => false,
            'message' => 'Manifiestos Pendientes obtenidas con exito',
            'count' => $manifiestos->count()
        ]);
    }

    public function detalle($carga) {
        $carga = Carga::where('id', $carga)->with('trackings')->withCount('manifiestos')->first();

        return response()->json([
            'data' => $carga,
            'error' => false,
            'message' => 'Detalle obtenido con exito'
        ]);

    }

    public function cargaZoom(Request $request)
    {
        if($request->hasFile('file'))
        {
            $file = $request->file('file');
            $archivo = $this->upload_global($file, 'importaciones');

            if ($xlsx = SimpleXLSX::parse('uploads/importaciones/'.$archivo))
            {
                $guiasAdd = 0;
                $r = 0;
                foreach( $xlsx->rows() as $row ) {
                    $r++;
                    if($r >= 3)
                    {
                        $nro = explode("-", $row[5]);
                        if($nro[0] == "SUR")
                        {
                            $guia = Guia::find($nro[1]);
                            if($guia != null)
                            {
                                $guia->codigo_zoom = $row[1];
                                $guia->save();
                                $guiasAdd++;
                            }
                        }
                        
                    }
                }
            }
        }

        return response()->json([
            'data' => $guiasAdd,
            'error' => false,
            'message' => 'Carga con exito'
        ]);
    }


    function upload_global($file, $folder)
    {
        $file_type = $file->getClientOriginalExtension();
        $destinationPath = public_path() . '/uploads/'.$folder;
        $destinationPathThumb = public_path() . '/uploads/'.$folder.'thumb';
        $filename = uniqid().'_'.time() . '.' . $file->getClientOriginalExtension();
        $url = '/uploads/'.$folder.'/'.$filename;

        if ($file->move($destinationPath.'/' , $filename)) {
            return $filename;
        }
    }


}
