<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCourierRequest;
use App\Http\Requests\UpdateCourierRequest;
use App\Models\Courier;
use App\Models\OficinaCourier;
use Illuminate\Http\Request;

class CourierController extends Controller
{
    public function index()
    {
        $oficinas = OficinaCourier::all();
        return response()->json([
            'data' => $oficinas,
            'message' => 'Oficinas Disponibles',
            'error' => false
        ]);
    }

    public function searchOficina(Request $request)
    {
        $campo = $request->campo;

        $busqueda = OficinaCourier::where('idCourier', $request->idCourier)->where($campo, 'like', '%'.$request->q.'%')->get();

        return response()->json([
            'data' => $busqueda,
            'message' => 'Oficinas Courier',
            'error' => false
        ]);
    }
}
