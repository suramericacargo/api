<?php

namespace App\Http\Controllers;

use App\Models\Empresa;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class EmpresaController extends Controller
{
    public function index()
    {
        return response()->json([
            'message' => 'Empresas obtenidas con exito',
            'data' => Empresa::all(),
            'error' => false
        ]);
    }
    public function store(Request $request)
    {
        $empresa = Empresa::create([
            'nombre' => strtoupper($request->nombre),
            'representante' => strtoupper($request->representante),
            'telefono' => strtoupper($request->telefono),
            'correo' => strtoupper($request->correo),
            'status' => 1,
        ]);

        $administrador = User::create([
            'idEmpresa' => $empresa->id,
            'name' => strtoupper($request->name),
            'lastName' => strtoupper($request->lastName),
            'phone' => $request->phone,
            'email' => strtoupper($request->email),
            'password' => Hash::make($request->password),
            'typeUser' => 2
        ]);

        $empresa->idAdministrador = $administrador->id;
        $empresa->save();

        return response()->json([
            'message' => 'Empresa y Administrador registrada con exito',
            'data' => $empresa,
            'error' => false
        ]);
    }

    public function getData(Empresa $empresa)
    {
        return response()->json([
            'message' => 'Empresa obtenido con exito',
            'data' => $empresa,
            'error' => false
        ]);
    }
    public function update(Request $request, Empresa $empresa)
    {
        $empresa->nombre = strtoupper($request->nombre);
        $empresa->representante = strtoupper($request->representante);
        $empresa->telefono = $request->telefono;
        $empresa->correo = strtoupper($request->correo);
        $empresa->save();

        return response()->json([
            'message' => 'Empresa actualizada con exito',
            'data' => Empresa::all(),
            'error' => false
        ]);
    }
    public function desactivar(Empresa $empresa)
    {
        $empresa->status = 0;
        $empresa->save();

        return response()->json([
            'message' => 'Empresa desactivada con exito',
            'data' => Empresa::all(),
            'error' => false
        ]);
    }

    public function activar(Empresa $empresa)
    {
        $empresa->status = 1;
        $empresa->save();

        return response()->json([
            'message' => 'Empresa activada con exito',
            'data' => Empresa::all(),
            'error' => false
        ]);
    }
}
