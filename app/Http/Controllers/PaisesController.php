<?php

namespace App\Http\Controllers;

use App\Models\Paises;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;

class PaisesController extends Controller
{
    public function index()
    {
        $paises = Paises::where('status', 1)->get();
        $paises2 = [];

        foreach ($paises as $pais) {
            $paises2[] = array(
                'label' => $pais->nombre,
                'value' => $pais->id
            );
        }

        return response()->json([
            'message' => 'Paises obtenidos',
            'data' => $paises2,
            'error' => false
        ]);
    }

    public function getNacionalidades()
    {
        $paises = DB::table('nacionalidad')->select('*')->get();
        $paises2 = [];

        foreach ($paises as $pais) {
            $paises2[] = array(
                'label' => $pais->GENTILICIO_NAC,
                'value' => $pais->GENTILICIO_NAC
            );
        }

        return response()->json([
            'message' => 'Nacionalidades obtenidos',
            'data' => $paises2,
            'error' => false
        ]);
    }

    public function list()
    {
        return response()->json([
            'message' => 'Paises obtenidos',
            'data' => Paises::all(),
            'error' => false
        ]);
    }

    public function obtenerBanderas()
    {
        $paises = Paises::where('bandera', null)->get();
        $client = new Client();
        foreach ($paises as $pais ) {
            $res = $client->request('get', 'https://restcountries.com/v3.1/alpha/'.$pais->iso);
            $data = json_decode($res->getBody());
            $pais->bandera = $data[0]->flag;
            $pais->save();
        }
    }

    public function activar(Paises $pais)
    {
        $pais->status = 1;
        $pais->save();

        return response()->json([
            'message' => 'Pais actualizado con exito',
            'data' => $pais,
            'error' => false
        ]);
    }
    public function desactivar(Paises $pais)
    {
        $pais->status = 0;
        $pais->save();

        return response()->json([
            'message' => 'Pais actualizado con exito',
            'data' => $pais,
            'error' => false
        ]);
    }
}
