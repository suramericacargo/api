<?php

namespace App\Http\Controllers;

use App\Models\Guia;
use App\Models\GuiaTracking;
use App\Models\Oficina;
use Illuminate\Http\Request;

class AjustesController extends Controller
{
    public function ajustesTrackings()
    {
        $guias = Guia::where('id', '!=', '4014')->get();

    }

    public function ajusteVolumento()
    {
        $guias = Guia::where('id', '!=', '4014')->get();

        foreach ($guias as $guia ) {
            $newPeso = $guia->alto * $guia->ancho * $guia->largo / 1000000;
            $guia->m3 = $newPeso;
            $guia->save();
        }
    }
}
