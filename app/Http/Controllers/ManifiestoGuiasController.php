<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreManifiestoGuiasRequest;
use App\Http\Requests\UpdateManifiestoGuiasRequest;
use App\Models\ManifiestoGuias;

class ManifiestoGuiasController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreManifiestoGuiasRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(ManifiestoGuias $manifiestoGuias)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(ManifiestoGuias $manifiestoGuias)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateManifiestoGuiasRequest $request, ManifiestoGuias $manifiestoGuias)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(ManifiestoGuias $manifiestoGuias)
    {
        //
    }
}
