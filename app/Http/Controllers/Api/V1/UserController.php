<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\V1\UserCollection;
use App\Http\Resources\V1\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function index()
    {
        return new UserCollection(User::latest()->paginate());
    }
    public function show(User $user)
    {
        return new UserResource($user);
    }
    public function login(Request $request)
    {
        if (!Auth::attempt($request->only('email', 'password'))) {
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);
        }

        return response()->json([
            'token' => $request->user()->createToken('app')->plainTextToken,
            'message' => 'Success'
        ]);
    }
    public function register(Request $request)
    {
        $user = User::create([
            'idOficina' => $request->idOficina,
            'name' => $request->name,
            'lastName' => $request->lastName,
            'phone' => $request->phone,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'typeUser' => $request->typeUser,
        ]);

        return response()->json([
            'message' => 'Usuario Registrado con exito',
            'data' => $user,
            'error' => false
        ]);
    }


    public function perfil(Request $request)
    {
        return $request->user();
    }
}
