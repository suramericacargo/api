<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        if (!Auth::attempt($request->only('email', 'password'))) {
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);
        }

        return response()->json([
            'token' => $request->user()->createToken('app')->plainTextToken,
            'message' => 'Success',
            'data' => $request->user()
        ]);
    }

    public function noAutorizado()
    {
        return response()->json([
            'error' => true,
            'message' => 'Inicia Sesion',
            'data' =>null
        ]);
    }
}
