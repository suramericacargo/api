<?php

namespace App\Http\Controllers;

use App\Exports\ManifiestosExport;
use App\Models\Guia;
use App\Models\GuiaTracking;
use App\Models\Manifiesto;
use App\Models\ManifiestoGuias;
use App\Models\ManifiestoTracking;
use App\Models\Oficina;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Spatie\FlareClient\Flare;

class ManifiestoController extends Controller
{
    public function index(Request $request)
    {
        // Administrador
        if($request->user()->typeUser == 1)
        {
            $manifiestos = Manifiesto::orderBy('id', 'desc')->withCount('guias')->with('oficina')->get();
        }

        // Administrador
        if($request->user()->typeUser == 2)
        {
            $manifiestos = Manifiesto::where('idEmpresa', $request->user()->idEmpresa)->withCount('guias')->with('oficina')->get();
        }

        // Empleado
        if($request->user()->typeUser == 3)
        {
            $manifiestos = Manifiesto::where('idUsuario', $request->user()->id)->withCount('guias')->with('oficina')->get();
        }

        return response()->json([
            'data' => $manifiestos,
            'error' => false,
            'message' => 'Manifiestos obtenidas con exito'
        ]);
    }
    public function store(Request $request)
    {
        // Crea el manifiesto
        $manifiesto = Manifiesto::create([
            'idEmpresa' => $request->user()->idEmpresa,
            'idUsuario' => $request->user()->id,
            'idOficina' => $request->user()->idOficina,
            'tipoGuia' => $request->tipoEnvio,
            'estado' => 'Pendiente',
            'fecha' => date('Y-m-d')
        ]);

        // Enlaza las guias
        foreach ($request->guias as $guia) {
                $guia = Guia::find($guia);
                $rel = ManifiestoGuias::create([
                    'idManifiesto' => $manifiesto->id,
                    'idGuia' => $guia->id
                ]);

                $guia->idManifiesto = $manifiesto->id;
                $guia->save();

                $this->trackingGuias('Almacén', $request->user()->id, $guia->id);
        }

        // Genera el tracking General
        $tracking = ManifiestoTracking::create([
            'idManifiesto' => $manifiesto->id,
            'estado' => 'Almacén'
        ]);

        return response()->json([
            'data' => $manifiesto,
            'error' => false,
            'message' => 'Manifiesto Nº: '.$manifiesto->id. ' generado con exito'
        ]);
    }

    public function guiasPendientes(Request $request, $tipoGuia)
    {
        //Super admin
        if($request->user()->typeUser == 1)
        {
            $user = "SAdmin";
            $guias = Guia::where('idManifiesto', null)->where('tipoGuia', $tipoGuia)->get();
        }

        // Administrador
        if($request->user()->typeUser == 2)
        {
            $user = "Admin";
            $guias = Guia::where('idEmpresa', $request->user()->idEmpresa)->where('idManifiesto', null)->where('tipoGuia', $tipoGuia)->get();
        }

        // Empleado
        if($request->user()->typeUser == 3)
        {
            $user = "Trabajador";
            $guias = Guia::where('idUsuario', $request->user()->id)->where('idManifiesto', null)->where('tipoGuia', $tipoGuia)->get();
        }

        return response()->json([
            'typeUser' => $user,
            'data' => $guias,
            'error' => false,
            'message' => 'Guias Pendientes obtenidas con exito',
            'count' => $guias->count()
        ]);
    }

    public function manifiestoPdf($manifiesto)
    {
        return Excel::download(new ManifiestosExport($manifiesto), 'manifiesto-'.$manifiesto.'.xlsx');
    }

    public function trackingGuias($estado, $user, $guia)
    {
        if($estado == 'Almacén') //manifiesto
        { $estado = ['estado' => 'Almacén', 'texto' => 'Su paquete ha sido envalijado en Peru, y en pronto será enviado a su destino Venezuela.']; }
        if($estado == 'Aduana origen') //carga
        { $estado = ['estado' => 'Aduana origen', 'texto' => 'Su paquete ha sido entregado a la aduana en Peru donde se le cargarán los aranceles de salida del país o aranceles. ']; }
        if($estado == 'En Transito') // al dia siguiente
        { $estado = ['estado' => 'En Transito', 'texto' => 'Su paquete paquete se encuentra en translado al destino en Venezuela.']; }
        if($estado == 'Almacén ADT')
        { $estado = ['estado' => 'Almacén ADT', 'texto' => 'Mercancía no despachada a la espera de recibir el transbordo hacia Venezuela.']; }
        if($estado == 'Aduana destino') // 2 dias aduana destino
        { $estado = ['estado' => 'Aduana destino', 'texto' => 'Su paquete ha llegado a la aduana en Venezuela donde se le cargarán los aranceles de ingreso al país y será tramitado para el envío al Centro de Distribución.']; }
        if($estado == 'Recibido en Distribución')
        { $estado = ['estado' => 'Recibido en Distribución', 'texto' => 'El paquete se encuentra en el Centro de Distribución para su envío a la Sucursal seleccionada.']; }
        if($estado == 'Transferido')
        { $estado = ['estado' => 'Transferido', 'texto' => 'Su paquete fue enviado a la sucursal de retiro. ']; }
        if($estado == 'Disponible')
        { $estado = ['estado' => 'Disponible', 'texto' => 'Ya puedes retirar su paquete. ']; }
        if($estado == 'Entregado')
        { $estado = ['estado' => 'Entregado', 'texto' => 'Su paquete ha sido entregado a su destinatario!. ']; }

        $tracking = GuiaTracking::create([
            'idUsuario' => $user,
            'idGuia' => $guia,
            'estado' => $estado['estado'],
            'descripcion' => $estado['texto'],
        ]);
    }

    public function agregarTracking(Request $request)
    {
        $tracking = ManifiestoTracking::create([
            'idManifiesto' => $request->idManifiesto,
            'estado' => $request->estado,
        ]);

        $guias = ManifiestoGuias::where('idManifiesto', $request->idManifiesto)->get();

        foreach ($guias as $guia ) {
            $this->trackingGuias($request->estado, $request->user()->id, $guia->idGuia);
        }

        return response()->json([
            'data' => $tracking,
            'error' => false,
            'message' => 'Trackign Agregado con exito!',
        ]);
    }

    public function detalle($manifiesto)
    {
        $manifiesto = Manifiesto::where('id', $manifiesto)->with('trackings')->withCount('guias')->first();

        return response()->json([
            'data' => $manifiesto,
            'error' => false,
            'message' => 'Detalle obtenido con exito'
        ]);
    }
}
