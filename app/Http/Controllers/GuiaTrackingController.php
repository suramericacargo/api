<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreGuiaTrackingRequest;
use App\Http\Requests\UpdateGuiaTrackingRequest;
use App\Models\GuiaTracking;

class GuiaTrackingController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreGuiaTrackingRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(GuiaTracking $guiaTracking)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(GuiaTracking $guiaTracking)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateGuiaTrackingRequest $request, GuiaTracking $guiaTracking)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(GuiaTracking $guiaTracking)
    {
        //
    }
}
