<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreDestinatarioRequest;
use App\Http\Requests\UpdateDestinatarioRequest;
use App\Models\Destinatario;
use Illuminate\Http\Request;

class DestinatarioController extends Controller
{
    public function index($cliente)
    {
        return response()->json([
            'data' => Destinatario::where('idCliente', $cliente)->get(),
            'message' => 'Destinatarios Obtenidos',
            'error' => false
        ]);
    }

    public function store(Request $request)
    {
        $destinatario = Destinatario::updateOrCreate(
            [
                'documento' => $request->documento,
            ],
            [
                'idEmpresa' => $request->user()->idEmpresa,
                'idTipoDocumento' => $request->tipoDocumento,
                'nombres' => strtoupper($request->nombre),
                'apellidos' => strtoupper($request->apellido),
                'telefono' => $request->telefono,
                'correo' => strtoupper($request->correo),
                'estado' => $request->estado,
                'ciudad' => strtoupper($request->ciudad),
                'direccion' => strtoupper($request->direccion),
                'nacionalidad' => $request->nacionalidad,
                'idCliente' => $request->idCliente
            ]
        );

        return response()->json([
            'data' => $destinatario,
            'message' => 'Destinatario Creado',
            'error' => false
        ]);
    }

    public function newGuia($cliente)
    {
        $clientes = Destinatario::where('idCliente', $cliente)->get();
        $clientes2 = [];
        foreach ($clientes as $cliente ) {
            $clientes2[] = array(
                'label' => $cliente->documento .' - '. $cliente->nombres .' '. $cliente->apellidos,
                'value' => $cliente->id
            );
        }

        return response()->json([
            'data' => $clientes2,
            'message' => 'Destinatarios obtenidos con exito!',
            'error' => false
        ]);
    }
}
