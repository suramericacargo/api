<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class TrabajadorController extends Controller
{
    public function index(Request $request)
    {
        return response()->json([
            'message' => 'Empleados obtenidos con exito',
            'data' => User::where('idEmpresa', $request->user()->idEmpresa)->get(),
            'error' => false
        ]);
    }

    public function store(Request $request)
    {
        $usuario = User::create([
            'idEmpresa' => $request->idEmpresa,
            'idOficina' => $request->idOficina,
            'name' => strtoupper($request->name),
            'lastName' =>strtoupper( $request->lastName),
            'phone' => $request->phone,
            'email' => strtoupper($request->email),
            'password' => Hash::make($request->password),
            'typeUser' => 3
        ]);

        return response()->json([
            'message' => 'Empleado registrado con exito',
            'data' => $usuario,
            'error' => false
        ]);
    }

    public function show(User $administrador)
    {
        return response()->json([
            'message' => 'Administrador encontrado con exito',
            'data' => $administrador,
            'error' => false
        ]);
    }

    public function update(Request $request, User $usuario)
    {
        $usuario->name = strtoupper($request->name);
        $usuario->lastName = strtoupper($request->lastName);
        $usuario->phone = $request->phone;

        if($request->password != null)
            $usuario->password = Hash::make($request->password);

        $usuario->save();

        return response()->json([
            'message' => 'Usuario actualizado con exito',
            'data' => $usuario,
            'error' => false
        ]);
    }
}
