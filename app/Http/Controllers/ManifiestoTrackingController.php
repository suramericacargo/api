<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreManifiestoTrackingRequest;
use App\Http\Requests\UpdateManifiestoTrackingRequest;
use App\Models\ManifiestoTracking;

class ManifiestoTrackingController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreManifiestoTrackingRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(ManifiestoTracking $manifiestoTracking)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(ManifiestoTracking $manifiestoTracking)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateManifiestoTrackingRequest $request, ManifiestoTracking $manifiestoTracking)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(ManifiestoTracking $manifiestoTracking)
    {
        //
    }
}
