<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreSubCategoriaRequest;
use App\Http\Requests\UpdateSubCategoriaRequest;
use App\Models\SubCategoria;
use Illuminate\Http\Request;

class SubCategoriaController extends Controller
{
    public function store(Request $request)
    {
        $sub = SubCategoria::create([
            'idEmpresa' => $request->user()->idEmpresa,
            'idCategoria' => $request->idCategoria,
            'subCategoria' => $request->subCategoria
        ]);

        return response()->json([
            'message' => 'Sub Categoria creada con exito',
            'data' => $sub,
            'error' => false
        ]);
    }

    public function update(UpdateSubCategoriaRequest $request, SubCategoria $subCategoria)
    {
        //
    }

    public function destroy(SubCategoria $subCategoria)
    {
        //
    }
}
