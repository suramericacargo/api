<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreGuiaArticuloRequest;
use App\Http\Requests\UpdateGuiaArticuloRequest;
use App\Models\GuiaArticulo;

class GuiaArticuloController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreGuiaArticuloRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(GuiaArticulo $guiaArticulo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(GuiaArticulo $guiaArticulo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateGuiaArticuloRequest $request, GuiaArticulo $guiaArticulo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(GuiaArticulo $guiaArticulo)
    {
        //
    }
}
