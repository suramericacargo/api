<?php

use App\Http\Controllers\AjustesController;
use App\Http\Controllers\Api\V1\LoginController;
use App\Http\Controllers\ExcelsController;
use App\Http\Controllers\PaisesController;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Api\V1\UserController as UserV1;
use App\Http\Controllers\CajaController;
use App\Http\Controllers\CargaController;
use App\Http\Controllers\CategoriaController;
use App\Http\Controllers\ClienteController;
use App\Http\Controllers\CourierController;
use App\Http\Controllers\DestinatarioController;
use App\Http\Controllers\EmpresaController;
use App\Http\Controllers\GuiaController;
use App\Http\Controllers\ManifiestoController;
use App\Http\Controllers\OficinaController;
use App\Http\Controllers\SubCategoriaController;
use App\Http\Controllers\TasaEnvioController;
use App\Http\Controllers\TrabajadorController;
use App\Models\Carga;
use App\Models\Courier;

Route::apiResource('v1/posts', UserV1::class)
  ->only(['index', 'show'])
  ->middleware('auth:sanctum');

Route::get('login', [LoginController::class, 'noAutorizado'])->name('login');
Route::post('login', [LoginController::class, 'login']);
Route::post('register', [UserV1::class, 'register']);
Route::post('inicio-sesion', [UserV1::class, 'login']);
Route::get('mi-perfil', [UserV1::class, 'perfil'])->middleware('auth:sanctum');
Route::get('paises', [PaisesController::class, 'index'])->middleware('auth:sanctum');
Route::get('nacionalidades', [PaisesController::class, 'getNacionalidades'])->middleware('auth:sanctum');
Route::get('paises-all', [PaisesController::class, 'list'])->middleware('auth:sanctum');
Route::get('activarPais/{pais}', [PaisesController::class, 'activar'])->middleware('auth:sanctum');
Route::get('desactivarPais/{pais}', [PaisesController::class, 'desactivar'])->middleware('auth:sanctum');

Route::middleware('auth:sanctum')->controller(EmpresaController::class)->prefix('empresas')->group(function () {
    Route::get('/', 'index');
    Route::post('/', 'store');
    Route::post('/update/{empresa}', 'update');
    Route::get('/getData/{empresa}', 'getData');
    Route::get('/activar/{empresa}', 'activar');
    Route::get('/desactivar/{empresa}', 'desactivar');
});


Route::middleware('auth:sanctum')->controller(TrabajadorController::class)->prefix('trabajadores')->group(function () {
    Route::get('/', 'index');
    Route::post('/', 'store');
    Route::post('/update/{usuario}', 'update');
    Route::get('/getData/{administrador}', 'show');
    Route::get('/activar/{empresa}', 'activar');
    Route::get('/desactivar/{empresa}', 'desactivar');
});

Route::middleware('auth:sanctum')->controller(OficinaController::class)->prefix('oficinas')->group(function () {
    Route::get('/', 'index');
    Route::get('create', 'oficinasNew');
    Route::post('/', 'store');
    Route::get('/update/{empresa}', 'update');
    Route::get('/activar/{empresa}', 'activar');
    Route::get('/desactivar/{empresa}', 'desactivar');
});


Route::middleware('auth:sanctum')->controller(TasaEnvioController::class)->prefix('tasas-envio')->group(function () {
    Route::get('/', 'index');
    Route::get('/crear/{tipoGuia}', 'crear');
    Route::post('/', 'store');
    Route::get('/show/{tasaEnvio}', 'show');
    Route::post('/update/{tasaEnvio}', 'update');
});

Route::middleware('auth:sanctum')->controller(ClienteController::class)->prefix('clientes')->group(function () {
    Route::get('/', 'index');
    Route::get('/detalle/{cliente}', 'show');
    Route::post('/', 'store');
    Route::post('/update', 'update');
    Route::get('newGuia', 'newGuia');
});

Route::middleware('auth:sanctum')->controller(DestinatarioController::class)->prefix('destinatarios')->group(function () {
    Route::get('/{cliente}', 'index');
    Route::post('/', 'store');
    Route::get('newGuia/{cliente}', 'newGuia');
});

Route::middleware('auth:sanctum')->controller(CourierController::class)->prefix('couriers')->group(function () {
    Route::get('/', 'index');
});

Route::middleware('auth:sanctum')->controller(GuiaController::class)->prefix('guias')->group(function () {
    Route::post('/', 'store');
    Route::get('/dashboard/{tipoGuia}', 'dashboard');
});

Route::middleware('auth:sanctum')->controller(CajaController::class)->prefix('cajas')->group(function () {
    Route::get('/', 'index');
    Route::post('/', 'aperturarCaja');
    Route::post('/cierre', 'cerrarCaja');
    Route::get('/dashboard/{tipoGuia}', 'dashboard');
    Route::get('/tasa', 'getTasa');
    Route::get('/detalle/{caja}', 'detalle');
    Route::post('/transaccion', 'transaccion');
});


Route::middleware('auth:sanctum')->controller(ManifiestoController::class)->prefix('manifiestos')->group(function () {
    Route::get('/', 'index');
    Route::get('guiasPendientes/{tipoGuia}', 'guiasPendientes');
    Route::post('generate', 'store');
    Route::post('agregarTracking', 'agregarTracking');
    Route::get('detalle/{manifiesto}', 'detalle');
});


Route::middleware('auth:sanctum')->controller(CargaController::class)->prefix('cargas')->group(function () {
    Route::get('/', 'index');
    Route::post('/', 'store');
    Route::get('manifiestosPendientes/{tipoManifiesto}', 'manifiestosPendientes');
    // Route::post('agregarTracking', 'agregarTracking');
    Route::get('detalle/{carga}', 'detalle');
});

Route::post('cargas/carga-masiva', [CargaController::class, 'cargaZoom']);
// PDFS
Route::get('/guias/etiqueta/{guia}', [GuiaController::class, 'etiquetaPdf']);
Route::get('/guias/ticket/{guia}', [GuiaController::class, 'TicketPdf']);
Route::get('/guias/guia/{guia}', [GuiaController::class, 'guiaPdf']);

// Excels
Route::get('/pdf/manifiesto/{manifiesto}', [ManifiestoController::class, 'manifiestoPdf']);

// Trackign
Route::post('services-web/tracking-guia', [GuiaController::class, 'getGuia']);
Route::get('ajustesTrackings', [AjustesController::class, 'ajustesTrackings']);
Route::get('ajusteVolumento', [AjustesController::class, 'ajusteVolumento']);


Route::get('obtenerBanderas', [PaisesController::class, 'obtenerBanderas']);
