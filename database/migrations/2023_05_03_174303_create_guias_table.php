<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('guias', function (Blueprint $table) {
            $table->id();
            $table->string('tipoGuia');
            $table->integer('idUsuario');
            $table->integer('idEmpresa');
            $table->integer('idOficina');
            $table->integer('idCliente');
            $table->integer('idDestinatario');
            $table->integer('idCourier');
            $table->integer('idOficinaCourier');
            $table->integer('idTasa');
            $table->integer('idManifiesto')->nullable();
            $table->integer('idCarga')->nullable();
            $table->integer('idBulto')->nullable();
            $table->string('guia_courier')->nullable();
            $table->integer('alto');
            $table->integer('ancho');
            $table->integer('largo');
            $table->float('peso_volumetrico');
            $table->float('m3');
            $table->float('ft3');
            $table->float('peso');
            $table->float('costo');
            $table->string('divisa');
            $table->string('divisaSimbolo');
            $table->string('paisOrigen')->default('Peru');
            $table->string('paisDestino')->default('Venezuela');
            $table->float('montoSeguro')->default(0);
            $table->integer('entregaPP');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('guias');
    }
};
