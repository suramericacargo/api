<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tasa_envios', function (Blueprint $table) {
            $table->id();
            $table->integer('idEmpresa');
            $table->integer('idPais');
            $table->string('divisa');
            $table->string('divisaSimbolo');
            $table->integer('tipoEnvio');
            $table->float('tasa');
            $table->integer('precioFijo')->default(0);
            $table->integer('precioCalculado')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tasa_envios');
    }
};
