<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('destinatarios', function (Blueprint $table) {
            $table->id();
            $table->integer('idCliente');
            $table->integer('idEmpresa');
            $table->integer('idTipoDocumento');
            $table->integer('documento');
            $table->string('nombres');
            $table->string('apellidos');
            $table->string('telefono');
            $table->string('correo')->nullable();
            $table->string('estado');
            $table->string('ciudad');
            $table->string('direccion');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('destinatarios');
    }
};
