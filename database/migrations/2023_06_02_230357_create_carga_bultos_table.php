<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('carga_bultos', function (Blueprint $table) {
            $table->id();
            $table->integer('idCarga');
            $table->string('precinto');
            $table->integer('manifiestos');
            $table->double('alto');
            $table->double('ancho');
            $table->double('largo');
            $table->double('peso');
            $table->double('pesoVolumetrico');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('carga_bultos');
    }
};
