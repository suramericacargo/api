<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('cajas', function (Blueprint $table) {
            $table->float('montoDolaresInicial')->nullable();
            $table->float('montoDolaresFinal')->nullable();
            $table->float('montoSolesInicial')->nullable();
            $table->float('montoSolesFinal')->nullable();
            $table->float('ingresosSoles')->nullable();
            $table->float('ingresosDolares')->nullable();
            $table->float('egresosSoles')->nullable();
            $table->float('egresosDolares')->nullable();
            $table->float('tasaDolares')->nullable();
        });

        Schema::table('caja_registros', function (Blueprint $table) {
            $table->float('tipoMoneda')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
