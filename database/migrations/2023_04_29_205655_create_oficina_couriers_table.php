<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('oficina_couriers', function (Blueprint $table) {
            $table->id();
            $table->integer('idCourier');
            $table->string('codigo', 10);
            $table->string('pais', 30);
            $table->string('estado', 30);
            $table->string('ciudad', 30);
            $table->text('direccion');
            $table->string('razon_social');
            $table->string('rif');
            $table->string('telefonos');
            $table->string('contacto');
            $table->string('horario_atencion');
            $table->string('correo');
            $table->string('observacion');
            $table->integer('entrega_pp');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('oficina_couriers');
    }
};
