<style>
    @page {
        margin: 0cm 0.3cm 1cm 0.3cm;
        font-size: 8
    }

    body {
        margin-top: 2cm;
        margin-left: 0cm;
        margin-right: 0cm;
        margin-bottom: 1cm;
    }

    header {
        position: fixed;
        top: 0cm;
        left: 0cm;
        right: 0cm;
        height: 2cm;
        color: black;
        text-align: center;
        line-height: 1.5cm;
    }

    table {
        width: 100%
    }

    thead {
        background-color: gray;
        color: black;
    }

    thead>tr {
        background-color: gray;
        color: black;
    }

    thead>tr>td {
        background-color: gray;
        color: black;
    }
</style>

<body>
    <header>
        <table style="width: 100%">
            <tr style="width: 100%">
                <td style="width: 90%; font-size: 8">Manifiesto {{ '#' . $manifiesto->id }}</td>
                <td style="width: 20%; text-align:left!important; font-size: 8">{{ date('d/m/Y H:i:s A') }}</td>
            </tr>
        </table>
    </header>

    <table style="width: 100%">
        <thead>
            <tr>
                <td>Guia</td>
                <td>Courier</td>
                <td>Destino</td>
                <td>Remintente</td>
                <td>Destinatario</td>
                <td>N Doc</td>
                <td>Telefono</td>
                <td>Contenido</td>
                <td>Peso KG</td>
                <td>Peso VOL</td>
                <td>M3</td>
                <td>FT3</td>
                <td>VFOB</td>
                <td>Seguro</td>
            </tr>
        </thead>
        <tbody>
            @foreach ($manifiesto->guias as $guia)
                <tr>
                    <td>{{ $guia->guia->oficina->codigo }}-{{ $guia->idGuia }}</td>
                    <td>
                        @if ($guia->guia->entregaPP == 1)
                            Entrega Puerta a Puerta
                        @else
                            ZOOM
                        @endif
                    </td>
                    <td>
                        @if ($guia->guia->entregaPP == 1)
                            {{ $guia->guia->destinatario->direccion }}
                        @else
                            {{ $guia->guia->oficinaCourier->direccion }}
                        @endif
                    </td>
                    <td>{{ $guia->guia->cliente->nombres }} {{ $guia->guia->cliente->apellidos }}</td>
                    <td>{{ $guia->guia->destinatario->nombres }} {{ $guia->guia->destinatario->apellidos }}</td>
                    <td>{{ $guia->guia->destinatario->documento }}</td>
                    <td>{{ $guia->guia->destinatario->telefono }}</td>
                    <td>
                        @foreach ($guia->guia->articulos as $articulo)
                            ({{ $articulo->cantidad }}) {{ $articulo->producto }} <br>
                        @endforeach
                    </td>
                    <td>{{ $guia->guia->peso }}</td>
                    <td>{{ $guia->guia->peso_volumetrico }}</td>
                    <td>{{ $guia->guia->m3 }}</td>
                    <td>{{ $guia->guia->ft3 }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
