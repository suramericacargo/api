<table style="width: 100%">
    <tr style="width: 100%">
        <td style="width: 20">
            <h3 class="text-uppercase">{{ $empresa->nombre }}</h3>
        </td>
        <td style="width: 80">
            <didv class="text-right">
                <h1>Guia: {{ $guia->oficina->codigo }}-{{ $guia->id }}</h1>
            </didv>
        </td>
    </tr>
</table>
<div class="contenido">
    <table style="width: 100%">
        <tr style="width: 100%" style="background-color: gray; color:white;">
            <td style="width: 50; text-align: center; font-weight: 900">ENVIADO POR:</td>
            <td style="width: 50; text-align: center; font-weight: 900">RECIBE:</td>
        </tr>
        <tr>
            <td style="width: 50; text-align: center; text-transform: uppercase">{{ $guia->cliente->documento }} - {{ $guia->cliente->nombres }} {{ $guia->cliente->apellidos }}</td>
            <td style="width: 50; text-align: center; text-transform: uppercase">{{ $guia->destinatario->documento }} - {{ $guia->destinatario->nombres }} {{ $guia->destinatario->apellidos }}</td>
        </tr>
    </table>

    <table style="width: 100%; margin-top: 2rem">
        <tr style="width: 100%" style="background-color: gray; color:white;">
            <td style="width: 30; text-align: center; font-weight: 900">PESO:</td>
            <td style="width: 30; text-align: center; font-weight: 900">PESO VOLUMETRICO:</td>
            <td style="width: 40; text-align: center; font-weight: 900">MEDIDAS:</td>
        </tr>
        <tr>
            <td style="width: 50; text-align: center;">{{ $guia->peso }} KG</td>
            <td style="width: 50; text-align: center;">{{ $guia->peso_volumetrico }}</td>
            <td style="width: 50; text-align: center;">{{ $guia->alto }} * {{ $guia->ancho }} * {{ $guia->largo }}</td>
        </tr>
    </table>

    <table style="width: 100%; margin-top: 2rem">
        <tr style="width: 100%" style="background-color: gray; color:white;">
            <td style="width: 30; text-align: center; font-weight: 900">Courier Destino:</td>
            <td style="width: 70; text-align: center; font-weight: 900">Oficina Destino:</td>
        </tr>
        <tr>
            <td style="width: 50; text-align: center; text-transform: uppercase">
            @if($guia->entregaPP != 1)
            ZOOM - {{ $guia->oficinaCourier->codigo }}
            @else
            ENTREGA PUERTA A PUERTA
            @endif
            </td>
                <td style="width: 50; text-align: center; text-transform: uppercase; text-align: justify;">
                    @if($guia->entregaPP != 1)
                 ({{ $guia->oficinaCourier->pais }} - {{ $guia->oficinaCourier->estado }} - {{ $guia->oficinaCourier->ciudad }})
                {{ $guia->oficinaCourier->direccion }}
                @else
                {{ $guia->destinatario->direccion }}
                @endif
            </td>
        </tr>
    </table>


    <footer>
        URL DE VERIFICACION: <a href="https://www.suramericacargo.com/tracking/{{ $guia->id }}">https://www.suramericacargo.com/tracking/{{ $guia->id }}</a>
        <br>
        Telefonos Empresa: {{ $empresa->telefono }} / Oficina: {{ $guia->oficina->telefono }}
    </footer>


</div>
<style>
    html {
    	margin: 1rem 3rem;
    }

    .contenido{
        width: 100%;
        height: 80%;
        text-align: center;
    }

    .text-right{
        width: 100%;
        text-align: right;
    }

    footer {
        position: fixed;
        bottom: 60px;
        left: 0px;
        right: 0px;
        height: 50px;
        text-align: left;
        line-height: 35px;
    }

    .text-uppercase{
        text-transform: uppercase
    }
</style>
