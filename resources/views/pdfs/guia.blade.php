<html>

<head>
    <style>
        @page {
            margin: 0cm 1cm;
            font-size: 8
        }

        body {
            margin-top: 2cm;
            margin-left: 2cm;
            margin-right: 2cm;
            margin-bottom: 2cm;
        }

        header {
            position: fixed;
            top: 0cm;
            left: 0cm;
            right: 0cm;
            height: 2cm;
            color: black;
            text-align: center;
            line-height: 1.5cm;
        }

        .text-titulo {
            width: 100%;
            font-size: 2rem;
            font-weight: 700;
            text-align: center
        }

        .text-subtitulo {
            width: 100%;
            font-size: 10;
            text-align: center;
            text-transform: uppercase;
        }

        .hr {
            width: 100%
        }

        .row {
            width: 100;
        }

        .text-right {
            width: 100%;
            text-align: right !important;
        }

        .text-uppercase {
            text-transform: uppercase
        }

        .observacion-titulo {
            width: 100%;
            font-size: 10;
            text-align: center;
            text-transform: uppercase;
            font-weight: 600;
            margin-top: 1rem;
            text-decoration: underline
        }

        .observacion-contenido {
            text-align: justify;
            margin-top: 0.5rem
        }

        .firma {
            width: 100%;
            text-align: center;
            font-weight: 600;
            font-size: 10;
            margin-top: 5rem
        }


        .titulo2 {
            font-weight: 800;
            font-size: 3rem;
            width: 100%;
            text-align: center
        }

        .subtitulo2 {
            font-weight: 700;
            font-size: 2rem;
            width: 100%;
            text-align: center
        }

        .description {
            width: 100%;
            text-align: justify !important;
            padding-top: 1rem
        }

        tr.border_bottom td {
            border-bottom: 1px solid black;
        }

        .img-drogas{
            width: 100%;
            margin-top: 2rem;
        }
    </style>
</head>

<body>
    <header>
        <table style="width: 100%">
            <tr style="width: 100%">
                <td style="width: 90%; font-size: 8">Emision Guia</td>
                <td style="width: 20%; text-align:left!important; font-size: 8">{{ $guia->created_at }}</td>
            </tr>
        </table>
    </header>

    <main>
        <div class="text-titulo">
            Guia: {{ $guia->oficina->codigo ."-". $guia->id }}
        </div>
        <div class="text-subtitulo">
            (OFICINA: {{ $guia->oficina->nombre }} - {{ $guia->oficina->ciudad }})
        </div>

        <hr class="hr">

        <table style="width: 100%">
            <tr style="width: 100%">
                <td style="width: 50%">Peso En Kilos: {{ $guia->peso }} KG</td>
                <td style="width: 50%">
                    <div class="text-right">Peso Volumetrico: {{ $guia->peso_volumetrico }}</div>
                </td>
            </tr>

            <tr style="width: 100%">
                <td style="width: 50%">Medidas: {{ $guia->alto }}CM {{ $guia->ancho }}CM {{ $guia->largo }}CM</td>
                <td style="width: 50%">
                    <div class="text-right">Medidas en Pulgadas: {{ number_format($guia->alto / 2.54, 2) }}"x
                        {{ number_format($guia->ancho / 2.54, 2) }}\"x {{ number_format($guia->largo / 2.54, 2) }}"
                    </div>
                </td>
            </tr>

            <tr style="width: 100%">
                <td style="width: 50%">Pie Cubico: {{ number_format($guia->ft3, 4) }}</td>
                <td style="width: 50%">
                    <div class="text-right">M3: {{ number_format($guia->m3, 3) }}</div>
                </td>
            </tr>
        </table>

        <hr class="hr">

        <table style="width: 100%">
            <tr style="width: 100%">
                <td style="width: 50%" class="">Tipo de Envio: <span class="text-uppercase">{{ $guia->tipoGuia }}
                    </span>
                </td>
            </tr>
            <tr style="width: 100%">
                <td style="width: 50%" class="">Pais Envio: <span class="text-uppercase">{{ $guia->paisOrigen }}
                    </span>
                </td>
                <td style="width: 50%">
                    <div class="text-right ">Pais Destino: <span class="text-uppercase">{{ $guia->paisDestino }}</span>
                    </div>
                </td>
            </tr>

            <tr style="width: 100%">
                <td style="width: 100%">Remitente: <span class="text-uppercase ">{{ $guia->cliente->nombres }}
                        {{ $guia->cliente->apellidos }}</span>
                </td>
            </tr>

            <tr style="width: 100%">
                <td style="width: 50%">Tipo Documento:
                    @if ($guia->cliente->idTipoDocumento == 1)
                        CEDULA
                    @elseif($guia->cliente->idTipoDocumento == 2)
                        CARNET DE EXTRANJERIA
                    @elseif($guia->cliente->idTipoDocumento == 3)
                        DNI
                    @elseif($guia->cliente->idTipoDocumento == 4)
                        CPP/PTP
                    @elseif($guia->cliente->idTipoDocumento == 5)
                        PASAPORTE
                    @endif
                </td>
                <td style="width: 50%">
                    <div class="text-right">Numero de Documento: {{ $guia->cliente->documento }}</div>
                </td>
            </tr>

            <tr style="width: 100%">
                <td style="width: 70%">Destinatario: <span class="text-uppercase ">{{ $guia->destinatario->nombres }}
                        {{ $guia->destinatario->apellidos }}</span>
                </td>
                <td style="width: 30%">
                    <div class="text-right">Telefono: {{$guia->destinatario->telefono}}</div>
                </td>
            </tr>

            <tr style="width: 100%">
                <td style="width: 50%">Tipo Documento:
                    @if ($guia->destinatario->idTipoDocumento == 1)
                        CEDULA
                    @elseif($guia->destinatario->idTipoDocumento == 2)
                        CARNET DE EXTRANJERIA
                    @elseif($guia->destinatario->idTipoDocumento == 3)
                        DNI
                    @elseif($guia->destinatario->idTipoDocumento == 4)
                        CPP/PTP
                    @elseif($guia->destinatario->idTipoDocumento == 5)
                        PASAPORTE
                    @endif
                </td>
                <td style="width: 50%">
                    <div class="text-right">Numero de Documento: {{ $guia->destinatario->documento }}</div>
                </td>
            </tr>
        </table>

        <hr class="hr">
        <table style="width: 100%">
            <tr style="width: 100%">
                <td style="width: 50%" class="">
                        Identificador de carga:
                            @if($guia->idCarga != null)
                                {{ $guia->idCarga }}
                            @else
                                NO ASIGNADA
                            @endif
                </td>
                <td style="width: 50%">
                    <div class="text-right ">
                        Identificador de Manifiesto:
                            @if($guia->idManifiesto != null)
                                {{ $guia->idManifiesto }}
                            @else
                                NO ASIGNADA
                            @endif
                    </div>
                </td>
            </tr>
            <tr style="width: 100%">
                <td style="width: 50%" class="">Courier: <span class="text-uppercase">
                @if ($guia->entregaPP == 1)
                Entrega Puerta a Puerta
                @else
                ZOOM
                @endif
                    </span>
                </td>
                <td style="width: 50%">
                    <div class="text-right ">Tipo de Entrega: @if ($guia->entregaPP == 1)
                            Puerta a Puerta
                        @else
                            Retiro de Oficina
                        @endif
                    </div>
                </td>
            </tr>
            @if ($guia->entregaPP != 1)
            <tr style="width: 100%">
                <td style="width: 50%" class="">Codigo Oficina: <span class="text-uppercase">
                        {{ $guia->oficinaCourier->codigo }}
                    </span>
                </td>
                <td style="width: 50%">
                    <div class="text-right ">Ciudad: {{ $guia->oficinaCourier->ciudad }}
                    </div>
                </td>
            </tr>
            @endif
        </table>

        <p>Direccion:
            @if ($guia->entregaPP != 1)
            Estado: {{ $guia->oficinaCourier->estado }} - Ciudad: {{ $guia->oficinaCourier->ciudad }} - {{ $guia->oficinaCourier->direccion }}
            @else
            Estado: {{ $guia->destinatario->estado }} - Ciudad: {{ $guia->destinatario->ciudad }}  - {{ $guia->destinatario->direccion }}
            @endif
        </p>

        <hr class="hr">
        <table style="width: 100%">
            <tr style="width: 100%; background-color: gray; border-bottom: solid 1px black">
                <td style="width: 80%; padding: 3 0 3 5">
                    <b>RESUMEN</b>
                </td>
                <td style="width: 20%; padding: 3 0 3 0">
                    <b>TOTAL</b>
                </td>
            </tr>

            <tr style="width: 100%">
                <td style="width: 80%; padding: 3 0 3 5">
                    <div class="text-uppercase">
                        @foreach ($guia->articulos as $articulo)
                            <b>
                                ({{ $articulo->cantidad }})
                                {{ $articulo->producto }}
                            </b>
                        @endforeach
                    </div>

                </td>
                <td style="width: 20%; padding: 3 0 3 0">
                     <?php
                        $valorTotal = 0;
                     ?>
                    @foreach ($guia->articulos as $articulo)
                        <?php
                            $valor = $articulo->cantidad * $articulo->monto;
                            $valorTotal = $valorTotal + $valor;
                        ?>
                    @endforeach

                    {{ $valorTotal }}
                </td>
            </tr>
        </table>

        <div class="observacion-titulo">OBSERVACION IMPORTANTE</div>
        <div class="observacion-contenido">Certifico bajo juramento que el contenido del presente envío entregado a
            {{ $guia->oficina->codigo }}-{{ $empresa->nombre }}. Se ajusta a lo declarado en la guía
            y me hago responsable, ante las autoridades nacionales y extranjeras por el contenido y valor declarado.
            Este envío cumple los parámetros
            aduaneros del país destino. Adicionalmente certifico que el envió no tiene dinero, valores negociables, ni
            objetos de prohibido transporte,
            según las normas internacionales y legislación aplicable en el país destino u origen. Puede Existir retraso
            en la llegada en la cual acepto.</div>



        <div class="firma">
            _________________ <br> Firma Remitente
        </div>
    </main>



    <div style="page-break-after:always;"></div>

    <main>
        <div class="titulo2">ANEXO 1</div>
        <div class="subtitulo2">DECLARACION JURADA DE VALOR</div>

        <div class="description">
            Yo <span class="text-uppercase">{{ $guia->cliente->nombres }} {{ $guia->cliente->apellidos }}</span> de
            nacionalidad {{ $guia->cliente->nacionalidad }} , con documento de identidad N°
            {{ $guia->cliente->documento }}, domiciliado
            en {{ $guia->cliente->direccion }} - {{ $guia->cliente->ciudad }} - {{ $guia->cliente->estado }}, en
            merito a la Ley del Procedimiento Administrativo General Ley N° 27444, declaro el valor FOB estimado de la
            mercancia, asi como de los datos siguientes: GUIA: {{ $guia->oficina->codigo }}-{{ $guia->id }}
        </div>

        <table style="width: 100%; margin-top: 1rem">
            <tr style="width: 100%; background-color: gray; border-bottom: solid 1px black">
                <td style="width: 60%; padding: 3 0 3 5">
                    <b>Descr. y Caract. de la Mercancia</b>
                </td>
                <td style="width: 10%; padding: 3 0 3 0">
                    <b>Cant</b>
                </td>
                <td style="width: 10%; padding: 3 0 3 0">
                    <b>U. Med</b>
                </td>
                <td style="width: 10%; padding: 3 0 3 0">
                    <b>V. Unit</b>
                </td>
                <td style="width: 10%; padding: 3 0 3 0">
                    <b>V. FOB</b>
                </td>

            </tr>
            <?php $valor = 0;
            $suma = 0; ?>
            @foreach ($guia->articulos as $articulo)
                <tr class="border_bottom">
                    <td>{{ $articulo->producto }}</td>
                    <td>{{ $articulo->cantidad }}</td>
                    <td>UNIDAD</td>
                    <td>{{ $articulo->monto }}</td>
                    <td>{{ $valor = $articulo->cantidad * $articulo->monto }}</td>
                </tr>
                <?php $suma = $suma + $valor; ?>
            @endforeach
            <tr class="border_bottom">
                <td></td>
                <td></td>
                <td></td>
                <td>TOTAL</td>
                <td>{{ $suma }}</td>
            </tr>
        </table>

        <div class="description">
            Declaro bajo juramento que los presentes datos obedecen a la verdad, sometiendome a las sanciones
            administrativas, civiles y penales que
            correspondan en caso de falsedad de los mismo
        </div>

        <div class="firma">
            Lima {{ date('d-m-Y') }} <br><br><br>
            _________________ <br> Firma Remitente
        </div>
    </main>


    <div style="page-break-after:always;"></div>

    <main>
        <div class="titulo2">ANEXO 2</div>
        <div class="subtitulo2">CARTA ANTI-DROGA</div>

        <div class="description">

            Yo <span class="text-uppercase">{{ $guia->cliente->nombres }} {{ $guia->cliente->apellidos }}</span> , con
            @if ($guia->cliente->idTipoDocumento == 1)
                CEDULA
            @elseif($guia->cliente->idTipoDocumento == 2)
                CARNET DE EXTRANJERIA
            @elseif($guia->cliente->idTipoDocumento == 3)
                DNI
            @elseif($guia->cliente->idTipoDocumento == 4)
                CPP/PTP
            @elseif($guia->cliente->idTipoDocumento == 5)
                PASAPORTE
            @endif Nro. {{ $guia->cliente->documento }} de nacionalidad
            {{ $guia->cliente->nacionalidad }}
            manifiesto que la encomienda
            la cual decido enviar a traves de la empresa {{ $guia->oficina->codigo }}-{{ $empresa->nombre }} bajo numero de guia
            {{ $guia->oficina->codigo }}-{{ $guia->id }} declaro bajo fe de juramento que no se transporta ningun
            tipo de sustancia psicotropicas o estupefacientes señaladas en la Ley Organica de Drogas, asumiendo toda la
            responsabilidad del contenido de estos efectos, objetos, documentos u otros tipo de producto
        </div>

        <img src="https://api.suramericacargo.com/img/requerimientos-documentos/tabla-antidroga.png" class="img-drogas" alt="">
    </main>
</body>

</html>
