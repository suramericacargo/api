<style>
    /** Define the margins of your page **/
    @page {
        margin: 10 0 0 0;
    }

    header {
        position: fixed;
        top: -110px;
        left: 0px;
        right: 0px;
        height: 50px;

        color: white;
        text-align: center;
        line-height: 35px;
    }

    .titulos {
        width: 100%;
        background-color: #b9b9b9;
        font-weight: 600;
        text-align: center
    }

    .contenidos {
        width: 90%;
        margin-left: 5%;
        font-size: 10;
        text-align: left
    }

    .contenidosTerminos {
        width: 90%;
        margin-left: 5%;
        font-size: 8;
        text-align: justify
    }

    .contenidosTotal {
        width: 90%;
        margin-left: 5%;
        font-size: 19;
        text-align: center;
        font-weight: 700
    }

    .contenidosTotal {
        width: 90%;
        margin-left: 5%;
        font-size: 13;
        text-align: center;
        font-weight: 700
    }

    .text-uppercase{
        text-transform: uppercase
    }
</style>
</head>

<body>
    <main style="text-align: center">
        <h3 class="text-uppercase">{{ $empresa->nombre }}</h3>
        ({{ $guia->oficina->codigo }}) - {{ $guia->oficina->nombre }} <br>
        Telefono: {{ $guia->oficina->telefono }} <br>
        {{ $guia->oficina->horario_atencion }} <br>
        {{ $guia->oficina->direccion }} <br>

        <table style="width: 100%">
            <tr style="width: 100%">
                <td style="width: 50%; ">
                    <h3 style="padding-left: 1rem">Tracking:</h3>
                </td>
                <td style="width: 50%; text-align: right; padding-right: 1rem">
                    <h3>{{ $guia->oficina->codigo }}-{{ $guia->id }}</h3>
                </td>
            </tr>
            <tr style="width: 100%">
                <td style="width: 30%; ">
                    <span style="padding-left: 1rem">Fecha:</span>
                </td>
                <td style="width: 70%; text-align: right; padding-right: 1rem">
                    <span style="padding-left: 1rem">{{ $guia->created_at }}</span>
                </td>
            </tr>
        </table>
        <br>
        <div class="titulos">
            REMITENTE
        </div>

        <div class="contenidos">
            <b>Nombre: {{ $guia->cliente->nombres }} {{ $guia->cliente->apellidos }}</b> <br>
            <b>Nro. Documento:  {{ $guia->cliente->documento }}</b> <br>
            <b>Telefono: {{ $guia->cliente->telefono }}</b> <br>
        </div>

        <br>
        <div class="titulos">
            DESTINTARIO
        </div>
        <div class="contenidos">
            <b>Nombre: {{ $guia->destinatario->nombres }} {{ $guia->destinatario->apellidos }}</b> <br>
            <b>Nro. Documento:  {{ $guia->destinatario->documento }}</b> <br>
            <b>Telefono: {{ $guia->destinatario->telefono }}</b> <br>
        </div>

        <br>
        @if($guia->entregaPP == 0)
        <div class="titulos">
            COURIER
        </div>
        <div class="contenidos">
            <tr style="width: 100%">
                <td style="width: 50%" class="">Pais Envio: <span class="text-uppercase">{{ $guia->paisOrigen }}
                    </span>
                </td>
                <td style="width: 50%">
                    <div class="text-right ">Pais Destino: <span class="text-uppercase">{{ $guia->paisDestino }}</span>
                    </div>
                </td>
            </tr>

            <b>Entrega:</b>  ZOOM<br>
            <b>Direccion:</b> {{ $guia->oficinaCourier->ciudad }} - {{ $guia->oficinaCourier->direccion }} <br>
        </div>
        @else
        <div class="titulos">
            ENTREGA PUERTA
        </div>
        <div class="contenidos">
            Pais Envio: <span class="text-uppercase">{{ $guia->paisOrigen }}
            </span>
            <div class="text-right ">Pais Destino: <span class="text-uppercase">{{ $guia->paisDestino }}</span>
            </div>
            <b>Direccion:</b> {{ $guia->destinatario->direccion }}  <br>
        </div>
        @endif

        <br>
        <div class="titulos">
            PAQUETE
        </div>
        <div class="contenidos">
            <b>Tipo Envio:</b> <span class="text-uppercase">{{ $guia->tipoGuia }}</span><br>
            <b>Peso KG:</b> {{ $guia->peso }} <br>
            <b>Peso Vol:</b> {{ $guia->peso_volumetrico }} <br>
            <b>Dimensiones:</b> {{ $guia->alto }}cm x {{ $guia->ancho }}cm x {{ $guia->largo }}cm <br>
        </div>

        @if($guia->montoSeguro != 0)
        <br>
        <div class="titulos">
            SEGURO ENVIO
        </div>
        <div class="contenidosTotal">
            {{ $guia->divisaSimbolo }} {{ $guia->montoSeguro }}
        </div>
        @else
        <br>
        <div class="titulos">
            SEGURO ENVIO
        </div>
        <div class="contenidosTotal2">
            ENVIO NO ASEGURADO
        </div>
        @endif

        <br>
        <div class="titulos">
            COSTO ENVIO
        </div>
        <div class="contenidosTotal">
            {{ $guia->divisaSimbolo }} {{ $guia->costo }}
        </div>

        <br>
        <div class="titulos">
            Terminos y Condiciones
        </div>

        <div class="contenidosTerminos">
            ENTIENDO Y ACEPTO QUE "{{ $empresa->nombre }}",
            NO ASUMIRÁ RESPONSABILIDAD ALGUNA POR LOS
            ENVIOS DECOMISADOS BIEN SEA POR CONTENER
            OBJETOS PROHIBIDOS O SOMETIDOS A DERECHO DE
            ADUANA O CONFISCADO POR LAS AUTORIDADES. Asi
            mismo declaro conocer que este servicio tiene limitaciones
            impuestos por razones de conveniencia general en defensa
            moral y pública de seguridad nacional, defensa del tesoro
            público y también por razones de interés del propio servicio y
            de sus funciones. De acuerdo con el principio anterior, certifico
            que no estoy enviando ningúno de los siguientes articulos. *
            Objetos cuya admisión o circulación esté prohibida en el país
            destino. * Dinero en efectivo y otros objetos de valor como
            (moneda, oro, platino, plata, piedras preciosas). * Materiales
            explosivos, informales, virus,agentes irritables o peligrosos. *
            Todo aquello que los convenios o acuerdos internacionales
            consagran como prohibida circulación. Dicho esto también
            declaro haber sido notificado que "Todos los envíos
            internacionales están sujetos a revisión por parte de las
            autoridades en los países de origen, destino y transito y pueden
            llegar a generar pagos de impuestos en el país de destino y/o
            demorarse por seguridad, por lo tanto, me comprometo a
            presentar y pagar ante las autoridades correspondientes los
            impuestos que pudiense generar en las aduanas nacionales o
            coordinar con el destinatario para el pago de los impuestos
            aduanales en el país destino en caso que así lo requieran las las
            autoridades correspondientes"
        </div>
    </main>
</body>

</html>
