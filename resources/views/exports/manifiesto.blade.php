
<table style="width: 100%">
        <thead>
            <tr>
                <td>Guia</td>
                <td>Courier</td>
                <td>Destino</td>
                <td>Remintente</td>
                <td>Destinatario</td>
                <td>N Doc</td>
                <td>Telefono</td>
                <td>Contenido</td>
                <td>Peso KG</td>
                <td>Peso VOL</td>
                <td>M3</td>
                <td>FT3</td>
                <td>VFOB</td>
                <td>Alto</td>
                <td>Ancho</td>
                <td>Largo</td>
            </tr>
        </thead>
        <tbody>
            @foreach ($manifiesto->guias as $guia)
                <tr>
                    <td>{{ $guia->guia->oficina->codigo }}-{{ $guia->idGuia }}</td>
                    <td>
                        @if ($guia->guia->entregaPP == 1)
                            Entrega Puerta a Puerta
                        @else
                            {{ $guia->guia->courier->nombre }}
                        @endif
                    </td>
                    <td>
                        @if ($guia->guia->entregaPP == 1)
                            {{ $guia->guia->destinatario->direccion }}
                        @else
                            {{ $guia->guia->oficinaCourier->direccion }}
                        @endif
                    </td>
                    <td>{{ $guia->guia->cliente->nombres }} {{ $guia->guia->cliente->apellidos }}</td>
                    <td>{{ $guia->guia->destinatario->nombres }} {{ $guia->guia->destinatario->apellidos }}</td>
                    <td>{{ $guia->guia->destinatario->documento }}</td>
                    <td>{{ $guia->guia->destinatario->telefono }}</td>
                    <td>
                        <?php $fov =  0; ?>
                        @foreach ($guia->guia->articulos as $articulo)
                            ({{ $articulo->cantidad }})
                            {{ $articulo->producto }} <br>
                            <?php
                                $monto = $articulo->cantidad * $articulo->monto;
                                $fov += $monto;
                            ?>
                        @endforeach
                    </td>
                    <td>{{ $guia->guia->peso }}</td>
                    <td>{{ $guia->guia->peso_volumetrico }}</td>
                    <td>{{ $guia->guia->m3 }}</td>
                    <td>{{ $guia->guia->ft3 }}</td>
                    <td>{{ $fov }}</td>
                    <td>{{ $guia->guia->alto }}</td>
                    <td>{{ $guia->guia->ancho }}</td>
                    <td>{{ $guia->guia->largo }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
